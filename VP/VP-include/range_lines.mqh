/*
Copyright 2024 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Range Lines. © FXcoder

#include "go.mqh"

class CRangeLines
{
public:

	CGO *line_from; // left
	CGO *line_to;   // right

private:

	string prefix_;
	const color line_from_color_;            // Left border line color
	const color line_to_color_;              // Right border line color
	const ENUM_LINE_STYLE line_from_style_;  // Left border line style
	const ENUM_LINE_STYLE line_to_style_;    // Right border line style

public:

	void CRangeLines(string prefix):
		prefix_(prefix),
		line_from_color_(clrBlue),
		line_from_style_(STYLE_DASH),
		line_to_color_(clrCrimson),
		line_to_style_(STYLE_DASH)
	{
		line_from = new CGO(prefix_ + "-from");
		line_to   = new CGO(prefix_ + "-to");
	}

	void ~CRangeLines()
	{
		delete line_from;
		delete line_to;
	}

	void draw_line_from(datetime time_from)
	{
		if (!line_from.exists() || (line_from.type() != OBJ_VLINE))
			line_from.redraw(OBJ_VLINE, 0, time_from, 0);
		else
			line_from.time(time_from);

		line_from.fg_color(line_from_color_)
			.back(false)
			.style(line_from_style_)
			.width(1)
			.tooltip("VP: range start");
	}

	void draw_line_to(datetime time_to)
	{
		if (!line_to.exists() || (line_to.type() != OBJ_VLINE))
			line_to.redraw(OBJ_VLINE, 0, time_to, 0);
		else
			line_from.time(time_to);

		line_to.fg_color(line_to_color_)
			.back(false)
			.style(line_to_style_)
			.width(1)
			.tooltip("VP: range end");
	}

	void draw_range_lines(datetime time_from, datetime time_to)
	{
		draw_line_from(time_from);
		draw_line_to(time_to);
	}

	void enable_line_from()
	{
		line_from.selectable(true).hidden(false);
	}

	void enable_line_to()
	{
		line_to.selectable(true).hidden(false);
	}

	void enable_range_lines()
	{
		enable_line_from();
		enable_line_to();
	}

	void disable_line_from()
	{
		line_from.selectable(false).hidden(true);
	}

	void disable_line_to()
	{
		line_to.selectable(false).hidden(true);
	}

	void disable_range_lines()
	{
		disable_line_from();
		disable_line_to();
	}

	void delete_range_lines()
	{
		line_from.del();
		line_to.del();
	}
};
