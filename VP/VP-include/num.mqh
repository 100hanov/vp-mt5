/*
Copyright 2024 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Number. © FXcoder

#include "str.mqh"

class CNumberUtil
{
public:

	/*
	Компактно преобразовать число (double) в строку с указанием максимального числа знаков.
	Компактность достигается за счет удаление лишних завершающих нулей в дробной части. Может быть удобно для показа
	числовых данных пользователю, когда нужна максимальная точность, но без отображения лишней информации.
	@param d          Число
	@param digits     Максимальное количество знаков после запятой
	@return           Число в строковом формате без лишних завершающих нулей в дробной части.
	*/
	static string to_string_compact(double value, int digits = 8)
	{
		string s = DoubleToString(value, digits);

		// убрать нули в конце дробной части
		if (StringFind(s, ".") >= 0)
		{
			s = _str.trim_end(s, '0');
			s = _str.trim_end(s, '.');
		}

		return s;
	}

	static string to_string_compact(double value, int digits, uchar dot)
	{
		string s = to_string_compact(value, digits);

		if (dot == '.')
			return s;

		int p = StringFind(s, ".");
		if (p != -1)
			StringSetCharacter(s, p, dot);

		return s;
	}

	static string to_string_sign(double value, int digits, string zero_sign = "", string plus_sign = "+", string minus_sign = "-")
	{
		if (value == 0)
			return(zero_sign + DoubleToString(value, digits));

		if (value > 0)
			return(plus_sign + DoubleToString(value, digits));

		return(minus_sign + DoubleToString(fabs(value), digits));
	}

} _num;
