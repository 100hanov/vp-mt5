/*
Copyright 2024 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// VP view. © FXcoder

#include "../enum/hg_coloring.mqh"
#include "../enum/quantile.mqh"
#include "../arr.mqh"
#include "../chart.mqh"
#include "../color.mqh"
#include "../go.mqh"
#include "../num.mqh"
#include "../series.mqh"
#include "enum/vp_bar_style.mqh"
#include "enum/vp_range_sel.mqh"
#include "vp_hg_params.mqh"
#include "vp_histogram.mqh"
#include "vp_levels_params.mqh"

class CVPView
{
public:

	const bool show_hg;

private:

	const string id_;

	const CVPLevelsParams params_lvl_;
	const CVPHgParams     params_hg_;

	string quantile_names_[]; // for tooltips
	bool is_quantile_names_initialized;

	// auto colors
	color hg_color1_;
	color hg_color2_;
	color prev_background_color_;

public:

	void CVPView(
		string id,
		const CVPHgParams &params_hg,
		const CVPLevelsParams &params_lvl
		):
			prev_background_color_(clrNONE),
			hg_color1_(clrNONE),
			hg_color2_(clrNONE),

			id_(id),
			params_hg_(params_hg),
			params_lvl_(params_lvl),

			show_hg((params_hg_.coloring != HG_COLORING_NONE) && (_color.is_valid(params_hg_.color1) ||
				_color.is_valid(params_hg_.color2)))
	{
		is_quantile_names_initialized = init_quantile_names();
	}

	// draw horizon line
	void draw_horizon(string line_name, datetime hz_time)
	{
		string text = "VP: no data behind this line";
		CGO hz(line_name);
		hz.draw(OBJ_VLINE, 0, hz_time)
			.fg_color(clrRed)
			.width(1)
			.style(STYLE_DOT)
			.selectable(false)
			.hidden(true)
			.back(false)
			.text(text)
			.tooltip(text);
	}

	// draw histogram's bar
	void draw_bar(string name, datetime time1, datetime time2, double price,
		color line_color, int width, double hg_point, ENUM_VP_BAR_STYLE bar_style, ENUM_LINE_STYLE line_style, bool back, string tooltip = "\n")
	{
		CGO bar(name);

		if (bar_style == VP_BAR_STYLE_BAR)
		{
			bar.redraw(OBJ_RECTANGLE, 0, time1, price - hg_point / 2.0, time2, price + hg_point / 2.0);
		}
		else if ((bar_style == VP_BAR_STYLE_FILLED) || (bar_style == VP_BAR_STYLE_COLOR))
		{
			bar.redraw(OBJ_RECTANGLE, 0, time1, price - hg_point / 2.0, time2, price + hg_point / 2.0);
		}
		else if (bar_style == VP_BAR_STYLE_OUTLINE)
		{
			bar.redraw(OBJ_TREND, 0, time1, price, time2, price + hg_point);
		}
		else
		{
			bar.redraw(OBJ_TREND, 0, time1, price, time2, price);
		}

		const bool filled = bar_style == VP_BAR_STYLE_FILLED || bar_style == VP_BAR_STYLE_COLOR;
		set_bar_style(bar, line_color, width, bar_style, line_style, back, filled);
		bar.tooltip(tooltip);
	}

	void set_bar_style(CGO &bar, color line_color, int width, ENUM_VP_BAR_STYLE bar_style, ENUM_LINE_STYLE line_style, bool back, bool filled)
	{
		bar.hidden(true)
			.selectable(false)
			.style(line_style)
			.width(width)
			.fg_color(line_color)
			.ray_left(false)
			.ray_right(false)
			.back(back)
			.fill(filled);
	}

	void draw_level(string name, double price, string tooltip)
	{
		CGO level(name);
		level.redraw(OBJ_HLINE, 0, 0, price)
			.hidden(true)
			.selectable(false)
			.fg_color(params_lvl_.mode_level_color)
			.style(params_lvl_.mode_level_style)
			.width(params_lvl_.mode_level_style == STYLE_SOLID ? params_lvl_.mode_level_width : 1)
			.tooltip(tooltip)
			.back(false); // show price label on price scale
	}

	bool draw_hg(const CVPHistogram &hg, double zoom, double global_max_volume)
	{
		if (!is_quantile_names_initialized)
			return false;

		const int hg_size = ArraySize(hg.volumes);
		if (hg_size <= 0)
			return true;

		// get and check time range
		const datetime time_from = _series.time(hg.bar_from, false, true);
		const datetime time_to   = _series.time(hg.bar_to, false, true);
		if (time_from == 0 || time_to == 0)
			return false;

		// calculate time bars
		int tbars[];
		if (!_arr.resize(tbars, hg_size))
			return false;

		for (int i = 0; i < hg_size; i++)
			tbars[i] = _math.round_to_int(hg.bar_from - hg.volumes[i] * zoom);

		const bool is_outline = params_hg_.bar_style == VP_BAR_STYLE_OUTLINE;

		// remove zero tails
		int start_bar = 0;
		int end_bar = hg_size - 1;
		{
			while ((start_bar < hg_size) && (tbars[start_bar] == hg.bar_from))
				start_bar++;

			while ((end_bar >= 0) && (tbars[end_bar] == hg.bar_from))
				end_bar--;

			// outline style has an extra bar
			if (is_outline)
				start_bar--;
		}

		// init colors
		double color_levels[];
		const int colors_count = EnumHGColoringToLevels(params_hg_.coloring, hg.volumes, color_levels);
		color cl = hg_color1_;

		double next_volume = 0;
		const int first = is_outline ? -1 : 0;
		const int hg_point_norm_digits = _math.max(point_to_digits(hg.point, _Digits), 0);

		// `modes_set(hg.modes)` и `.UnionWith(hg.modes)` не работают (5.4260)
		CHashSet<int> modes_set;
		for (int i = ArraySize(hg.modes) - 1; i >= 0; --i)
			modes_set.Add(hg.modes[i]);

		for (int i = first; i < hg_size; i++)
		{
			const double price = NormalizeDouble(hg.low_price + i * hg.point, hg_point_norm_digits);
			const string price_string = DoubleToString(price, hg_point_norm_digits);
			const string name = hg.prefix + price_string;

			double volume = 0;
			int tbar1 = hg.bar_from;
			int tbar2 = hg.bar_to;
			int mode_bar2 = hg.bar_to;

			if (is_outline)
			{
				if (i <= first)
				{
					// below
					volume = 0;
					next_volume = hg.volumes[0];
					tbar1 = hg.bar_from;
					tbar2 = tbars[0];
				}
				else if (i < hg_size - 1)
				{
					volume = hg.volumes[i];
					next_volume = hg.volumes[i + 1];
					tbar1 = tbars[i];
					tbar2 = tbars[i + 1];
					mode_bar2 = tbar1;
				}
				else
				{
					// above
					volume = hg.volumes[i];
					next_volume = 0;
					tbar1 = tbars[i];
					tbar2 = hg.bar_from;
				}
			}
			else
			{
				volume = hg.volumes[i];

				if (params_hg_.bar_style == VP_BAR_STYLE_COLOR)
					tbar2 = _math.round_to_int(hg.bar_from - fabs(hg.bar_to - hg.bar_from) * zoom);
				else
					tbar2 = tbars[i];

				mode_bar2 = tbar2;
			}

			// skip zero tails
			if (i < start_bar || i > end_bar)
				continue;

			// tooltip for level or bar
			const string tooltip = (string)_math.round_to_long(volume) + " @ " + price_string;

			// Mode level
			if (params_lvl_.show_mode_level && _arr.contains(hg.modes, i))
				draw_level(name + " level", price, tooltip);

			const datetime t1 = _series.time(tbar1, false, true);
			const datetime t2 = _series.time(tbar2, false, true);

			// Draw only one bar on the same price using priority: quantiles, vwap_pos, max, mode.
			// If nothing fits, draw hg bar.

			if (show_hg)
			{
				cl = hg_color1_;
				double check_volume = is_outline ? fmax(volume, next_volume) : volume;

				for (int k = colors_count - 1; k >= 0; --k)
				{
					if (check_volume >= color_levels[k])
					{
						cl = _color.mix(hg_color1_, hg_color2_, double(k) / (colors_count - 1.0), 1);
						break;
					}
				}

				draw_bar(name, t1, t2, price, cl, params_hg_.line_width, hg.point, params_hg_.bar_style, STYLE_SOLID, true, tooltip);
			}

			const int qi = _arr.index_of(hg.quantiles, i);
			if (params_lvl_.show_quantiles && qi >= 0)
			{
				const datetime q_t2  = params_hg_.bar_style == VP_BAR_STYLE_COLOR ? t2 : time_to;
				draw_bar(
					name + " q." + (string)i,
					time_from, q_t2, price,
					params_lvl_.quantile_color, params_lvl_.stat_line_width, hg.point, VP_BAR_STYLE_LINE, params_lvl_.stat_line_style, false,
					quantile_names_[qi] + ", " + tooltip);
			}
			else if (params_lvl_.show_vwap && i == hg.vwap_pos)
			{
				const datetime vwap_t2  = params_hg_.bar_style == VP_BAR_STYLE_COLOR ? t2 : time_to;
				draw_bar(name + " vwap", time_from, vwap_t2, price, params_lvl_.vwap_color, params_lvl_.stat_line_width, hg.point, VP_BAR_STYLE_LINE, params_lvl_.stat_line_style, false, "VWAP " + tooltip);
			}
			else if (
				(params_lvl_.show_max && i == hg.max_pos) ||
				(params_lvl_.show_modes && modes_set.Contains(i)) // _arr.contains(hg.modes, i)
				)
			{
				const datetime mode_t2 = _series.time(mode_bar2, false, true);
				const bool is_max = params_lvl_.show_max && (i == hg.max_pos);
				const color mode_color = is_max ? params_lvl_.max_color : params_lvl_.mode_color;
				const string mode_tooltip = (is_max ? "max " : "mode ") + tooltip;

				if (params_hg_.bar_style == VP_BAR_STYLE_LINE)
				{
					draw_bar(name, time_from, mode_t2, price, mode_color, params_lvl_.mode_line_width, hg.point, VP_BAR_STYLE_LINE, STYLE_SOLID, false, mode_tooltip);
				}
				else if (params_hg_.bar_style == VP_BAR_STYLE_BAR)
				{
					draw_bar(name, time_from, mode_t2, price, mode_color, params_lvl_.mode_line_width, hg.point, VP_BAR_STYLE_BAR, STYLE_SOLID, false, mode_tooltip);
				}
				else if (params_hg_.bar_style == VP_BAR_STYLE_FILLED)
				{
					draw_bar(name, time_from, mode_t2, price, mode_color, params_lvl_.mode_line_width, hg.point, VP_BAR_STYLE_FILLED, STYLE_SOLID, false, mode_tooltip);
				}
				else if (params_hg_.bar_style == VP_BAR_STYLE_OUTLINE)
				{
					draw_bar(name + "+", time_from, mode_t2, price, mode_color, params_lvl_.mode_line_width, hg.point, VP_BAR_STYLE_LINE, STYLE_SOLID, false, mode_tooltip);
				}
				else if (params_hg_.bar_style == VP_BAR_STYLE_COLOR)
				{
					draw_bar(name, time_from, mode_t2, price, mode_color, params_lvl_.mode_line_width, hg.point, VP_BAR_STYLE_FILLED, STYLE_SOLID, false, mode_tooltip);
				}
			}
		}

		return true;
	}

	// Update colors if background color changed
	// returns true if any color changes
	bool update_auto_colors()
	{
		if (!show_hg)
			return false;

		const color new_bg_color = _chart.color_background();

		if (new_bg_color == prev_background_color_)
			return false;

		hg_color1_ = _color.validate(params_hg_.color1, new_bg_color);
		hg_color2_ = _color.validate(params_hg_.color2, new_bg_color);

		prev_background_color_ = new_bg_color;
		return true;
	}

private:

	// Получить количество знаков после запятой для указанного пункта.
	static int point_to_digits(double point, int max_digits)
	{
		if (point == 0)
			return max_digits;

		const string point_string = _num.to_string_compact(point, max_digits);
		const int point_string_len = StringLen(point_string);
		const int dot_pos = StringFind(point_string, ".");

		// point_string => result:
		//   1230   => -1
		//   123    =>  0
		//   12.3   =>  1
		//   1.23   =>  2
		//   0.123  =>  3
		//   .123   =>  3

		return dot_pos < 0
			? StringLen(_str.trim_end(point_string, '0')) - point_string_len
			: point_string_len - dot_pos - 1;
	}

	bool init_quantile_names()
	{
		double quantiles[];
		const int quantile_count = EnumQuantileToArray(params_lvl_.quantiles, quantiles);

		if (quantile_count < 0)
			return false;

		if (!_arr.resize(quantile_names_, quantile_count))
		{
			ArrayFree(quantile_names_);
			return false;
		}

		for (int i = 0; i < quantile_count; ++i)
			quantile_names_[i] = "P" + _num.to_string_compact(quantiles[i] * 100.0, 1);

		return true;
	}

};
