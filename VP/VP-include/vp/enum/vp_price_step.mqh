/*
Copyright 2024 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Price step. © FXcoder

enum ENUM_VP_PRICE_STEP
{
	VP_PRICE_STEP_1     = 1,    // 1 Point
	VP_PRICE_STEP_5     = 5,    // 5 Points
	VP_PRICE_STEP_10    = 10,   // 10 Points
	VP_PRICE_STEP_50    = 50,   // 50 Points
	VP_PRICE_STEP_100   = 100,  // 100 Points
};

int EnumVPPriceStepToValue(ENUM_VP_PRICE_STEP price_step)
{
	return (int)price_step;
}
