/*
Copyright 2024 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Histogram coloring. © FXcoder

#include "../arr.mqh"
#include "../math.mqh"

enum ENUM_HG_COLORING
{
	HG_COLORING_NONE              = 0x0100,  // No histogram
	HG_COLORING_FIRST             = 0x0200,  // 1st color only
	HG_COLORING_SECOND            = 0x0300,  // 2nd color only

	HG_COLORING_MEAN              = 0x1100,  // Split by mean
	HG_COLORING_Q1                = 0x1200,  // Split by Q1 (25%)
	HG_COLORING_Q2                = 0x1300,  // Split by Q2 (50%, median)
	HG_COLORING_Q3                = 0x1400,  // Split by Q3 (75%)

	HG_COLORING_QUARTILES         = 0x2100,  // Quartile gradient
	HG_COLORING_GRADIENT10        = 0x2200,  // Gradient (10 levels)
	HG_COLORING_GRADIENT50        = 0x2300,  // Gradient (50 levels)
};

bool EnumHGColoringIsMulticolor(ENUM_HG_COLORING hg_coloring)
{
	return
		hg_coloring != HG_COLORING_NONE &&
		hg_coloring != HG_COLORING_FIRST &&
		hg_coloring != HG_COLORING_SECOND;
}

int EnumHGColoringToLevels(ENUM_HG_COLORING hg_coloring, const double &values[], double &levels[])
{
	// Все типы цветового оформления представлены как градиент.

	const int max_count = 50;
	if (!_arr.resize(levels, max_count))
		return -1;

	switch (hg_coloring)
	{
		case HG_COLORING_NONE:
			return 0;

		case HG_COLORING_FIRST:
			levels[0] = _math.min(values) - 1; // совпадёт всегда
			levels[1] = _math.max(values) + 1; // никогда не совпадёт
			return 2;

		case HG_COLORING_SECOND:
			levels[0] = _math.max(values) + 1; // никогда не совпадёт
			levels[1] = _math.min(values) - 1; // совпадёт всегда
			return 2;

		case HG_COLORING_MEAN:
			levels[0] = _math.min(values);
			levels[1] = _math.mean(values);
			return 2;

		case HG_COLORING_Q1:
			{
				double quartiles[];
				if (!_math.quartiles(values, false, quartiles))
					return -1;

				levels[0] = 0;
				levels[1] = quartiles[0];
				return 2;
			}

		case HG_COLORING_Q2:
			levels[0] = _math.min(values);
			levels[1] = _math.median_or(values, _math.nan);
			return 2;

		case HG_COLORING_Q3:
			{
				double quartiles[];
				if (!_math.quartiles(values, false, quartiles))
					return -1;

				levels[0] = 0;
				levels[1] = quartiles[2];
				return 2;
			}

		case HG_COLORING_QUARTILES:
			{
				double quartiles[];
				if (!_math.quartiles(values, false, quartiles))
					return -1;

				levels[0] = 0;
				levels[1] = quartiles[0];
				levels[2] = quartiles[1];
				levels[3] = quartiles[2];
				return 4;
			}

		case HG_COLORING_GRADIENT10:
			{
				const double step = (_math.max(values) - _math.min(values)) / 10.0;

				for (int i = 0; i < 10; ++i)
					levels[i] = i * step;

				return 10;
			}

		case HG_COLORING_GRADIENT50:
			{
				const double step = (_math.max(values) - _math.min(values)) / 50.0;

				for (int i = 0; i < 50; ++i)
					levels[i] = i * step;

				return 50;
			}
	}

	return -1;
}
